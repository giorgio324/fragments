package com.example.navgiation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.navgiation.fragment.DashboardFragment
import com.example.navgiation.fragment.HomeFragment
import com.example.navgiation.fragment.NotificationsFragment
import com.example.navgiation.fragment.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {


    private val homeFragment = HomeFragment()
    private val notificationsFragment = NotificationsFragment()
    private val settingsFragment = SettingsFragment()
    private val dashboardFragment = DashboardFragment()





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(homeFragment)
        val bottomNavigationView: BottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.icon_home -> replaceFragment(homeFragment)
                R.id.icon_dashboard -> replaceFragment(dashboardFragment)
                R.id.icon_notification -> replaceFragment(notificationsFragment)
                R.id.icon_settings -> replaceFragment(settingsFragment)
            }
            true

        }



    }

    private fun replaceFragment(fragment:Fragment){
        if (fragment != null){

            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment)
            transaction.commit()

        }


    }



}